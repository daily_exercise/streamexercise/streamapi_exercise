import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SolutionFile {

        public static void main(String[] args) {
                Student student1 = new Student(
                                "Sajal",
                                20,
                                new Address("1234"),
                                Arrays.asList(new MobileNumber("1233"), new MobileNumber("1234")));

                Student student2 = new Student(
                                "Cat",
                                20,
                                new Address("1235"),
                                Arrays.asList(new MobileNumber("1111"), new MobileNumber("3333"),
                                                new MobileNumber("1233")));

                Student student3 = new Student(
                                "Bat",
                                20,
                                new Address("1236"),
                                Arrays.asList(new MobileNumber("3333"), new MobileNumber("4444")));

                List<Student> studentData = Arrays.asList(student1, student2, student3);

                // 01. Get student with exact match name "Sajal"
                Optional<String> nameJayesh = (studentData.stream()
                                .filter(s -> s.getName().equals("Sajal"))
                                .map(m -> m.getName())).findAny();
                System.out.println(nameJayesh.isPresent() ? "Found" : "Not Found");

                // 02. Get student with matching address "1235"
                Optional<String> address1235 = studentData.stream()
                                .filter(f -> f.getAddress().getZipcode().equals("1235"))
                                .map(m -> m.getName()).findFirst();
                address1235.ifPresent(System.out::println);

                // 03. Get all student having mobile numbers 3333
                List<Student> names3333 = studentData.stream()
                                .filter(f -> f.getMobileNumbers().stream()
                                                .anyMatch(x -> Objects.equals(x.getNumber(), "3333")))
                                .collect(Collectors.toList());
                names3333.forEach(x -> System.out.println(x.getName()));

                // 04. Get all student having mobile number 1233 and 1234
                List<Student> names12331234 = studentData.stream()
                                .filter(f -> f.getMobileNumbers().stream().allMatch(x -> Objects.equals(x.getNumber(),
                                                "1233") || Objects.equals(x.getNumber(), "1234")))
                                .collect(Collectors.toList());
                names12331234.forEach(x -> System.out.println(x.getName()));

                // 05. Create a List<Student> from the List<TempStudent>

                TempStudent tmp1Student = new TempStudent(
                                "TempuOne",
                                20,
                                new Address("1235"),
                                Arrays.asList(new MobileNumber("1111"), new MobileNumber("3333"),
                                                new MobileNumber("1233")));

                TempStudent tmp2Student = new TempStudent(
                                "TempuTwo",
                                20,
                                new Address("1236"),
                                Arrays.asList(new MobileNumber("3333"), new MobileNumber("4444")));

                List<TempStudent> studentListFromTempList = Arrays.asList(tmp1Student,
                                tmp2Student);

                List<Student> tempStudentsToStudentsList = studentListFromTempList.stream()
                                .map(m -> new Student(m.name, m.age, m.address, m.mobileNumbers))
                                .collect(Collectors.toList());

                // 06. Convert List<Student> to List<String> of student name
                List<String> studentNames = studentData.stream()
                                .map(Student::getName)
                                .collect(Collectors.toList());
                studentNames.forEach(System.out::println);

                // 07. Convert List<students> to String
                String listToStringConvert = studentData.stream()
                                .map(Student::toString).collect(Collectors.joining(","));
                System.out.println(listToStringConvert);

                // 08. Change the case of student names as List<String>
                Arrays.asList(student1.getName(), student2.getName(), student3.getName()).stream()
                                .map(String::toUpperCase)
                                .collect(Collectors.toList())
                                .forEach(System.out::println);

                // 09. Sort students names as List<String>
                Arrays.asList(student1.getName(), student2.getName(), student3.getName()).stream()
                                // .sorted() // accending Order
                                .sorted(Comparator.reverseOrder()) // Descending Order
                                .forEach(System.out::println);

                // 10. Conditionally apply Filter condition, say if flag is enabled then.

        }

}
